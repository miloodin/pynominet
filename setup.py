from setuptools import setup

setup(name='pyNominet',
      version='0.1',
      description='Nominet API Wrapper',
      url='https://gitlab.com/miloodin/pynominet',
      author='Flying Circus',
      author_email='dev@mylesmcsweeney.com',
      license='GNU3',
      packages=['pynominet'],
      zip_safe=False)
